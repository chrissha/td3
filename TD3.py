"""Implementation of TD3 reinforcement learning algorithm.

This file contains an implementation of TD3 reinforcement learning algorithm in pytorch. TD3 is an
actor-critic algorithm and an extension of DDPG. 

DDPG Paper: https://arxiv.org/pdf/1509.02971.pdf
TD3 Paper: https://arxiv.org/pdf/1802.09477.pdf
"""

import os
import copy
import random
from collections import deque 
from itertools import count

# Third party
import torch
import gym
import numpy as np
import matplotlib.pyplot as plt
from torch import nn
from gym.spaces import Discrete


class ReplayMemory():
    """Replay memory buffer manager class. 
    
    This class is used to store transitions in the environment each timestep which are later 
    sampled and used to update the actor and critic. Methods include put() to store a transition
    and sample() to sample random transitions.
    """
    def __init__(self, limit, device, seed=None):
        """Initialize the replay memory with the given capacity"""
        self.mem = deque(maxlen=limit)
        self.device = device
        if seed is not None:
            random.seed(seed)

    
    def put(self, obs, act, rew, done, new_obs):
        """Inserts a transition tuple (obs, action, rew, done, new_obs) into the replay memory.

        params:
            obs (np.array): The observation
            act (np.array): The action taken
            rew (float): The reward recieved for taking the action
            done (bool): If the episode is done after the action was taken
            new_obs (np.array): The new observation after the action was taken
        """
        transition = (obs, act, rew, done, new_obs)
        self.mem.append(transition)


    def size(self):
        """Returns the size of the replay memory
        
        returns:
            (int): The current size of the replay memory
        """
        return len(self.mem)


    def sample(self, n):
        """Samples n transitions randomly from the replay memory and returns the variables
        separated as 5 pytorch tensors.

        params:
            n (int): How many transitions to sample.
        returns:
            (torch.tensor): Tensor of observations
            (torch.tensor): Tensor of actions
            (torch.tensor): Tensor of rewards
            (torch.tensor): Tensor of done bools
            (torch.tensor): Tensor of observations after action was taken
        """
        sample = random.sample(self.mem, n)
        obs_lst, act_lst, rew_lst, done_lst, new_obs_lst = [], [], [], [], []

        for transition in sample:
            o, a, r, d, o_prime = transition
            obs_lst.append(o)
            act_lst.append(a)
            rew_lst.append(r)
            done_lst.append(d)
            new_obs_lst.append(o_prime)

        return torch.tensor(obs_lst, dtype=torch.float32, device=self.device), \
            torch.tensor(act_lst, dtype=torch.float32, device=self.device), \
            torch.tensor(rew_lst, device=self.device), \
            torch.tensor(done_lst, device=self.device), \
            torch.tensor(new_obs_lst, dtype=torch.float32, device=self.device)


class Mu(nn.Module):
    """Default actor network

    Mu (or µ) is the symbol that represents the actor network. The actor reads the state and
    returns an action.
    """
    def __init__(self, obs_space, act_space):
        super(Mu, self).__init__()
        self.fc1 = nn.Linear(obs_space, 400)
        self.fc2 = nn.Linear(400, 300)
        self.fc3 = nn.Linear(300, act_space)


    def forward(self, x):
        x = torch.relu(self.fc1(x))
        x = torch.relu(self.fc2(x))
        x = torch.tanh(self.fc3(x))
        return x


class Q(nn.Module):
    """Default critic network

    The critic is denoted Q and is the network that evaluates actions taken in a given state.
    """
    def __init__(self, obs_space, act_space):
        super(Q, self).__init__()
        self.fc_1 = nn.Linear(obs_space + act_space, 400)
        self.fc_2 = nn.Linear(400, 300)
        self.fc_3 = nn.Linear(300, 1)


    def forward(self, x, a):
        cat = torch.cat([x, a], dim=1)
        x = torch.relu(self.fc_1(cat))
        x = torch.relu(self.fc_2(x))
        x = self.fc_3(x).squeeze()
        return x


class TD3():
    """TD3 class that implements the TD3 algorithm.

    TD3 class includes functions for setting up the algorithm to learn with different kinds of
    environments. The model includes methods for training the agent and predicting actions given
    states.
    """
    def __init__(self, env, actor=None, critic=None, ep_limit=300, initial_steps=1000, 
        buffer_size=50000, batch_size=100, mu_lr=1e-3, Q_lr=1e-3, gamma=0.99, tau=0.005, sigma=0.2, 
        sigma_exp=0.1, noise_clip=0.5, delay_step=2, eval_freq=5000, seed=None):
        """
        Initialize a TD3 agent. Default parameters are based of whats used in the original paper.

        params:
            env (callable): A callable function to create the gym environment to train the model on
            actor (class): A class deriving from nn.module representing the actor
            critic (class): A class deriving from nn.module representing the critic
            ep_limit (int): Max number of timesteps to run the environment for each episode.
            initial_steps (int): Number of timesteps to run and collect transitions for before 
                                optimizing the actor and critic.
            buffer_size (int): Max size of the replay memory.
            batch_size (int): Batch size used. How many samples to use each update.
            mu_lr (float): Learning rate for the actor (denoted mu/µ).
            Q_lr (float): Leanring rate for the critic (denoted Q).
            gamma (float): Discount factor used in the critic update.
            tau (float): Parameter for soft update in the target networks.
            sigma (float): Parameter for action noise sampling during optimization
            sigma_exp (float): Same as sigma, but used for action noise in exploration.
            noise_clip (float): The clip range for action estimate noise (-c, c).
            delay_step (int): Update policy and target network every 'delay_step' timestep.
            eval_freq (int): How often to _ the agent (in timesteps) and log average reward.
        """
        if seed is not None:
            random.seed(seed)
            np.random.seed(seed)
            torch.manual_seed(seed)

        # Hyperparameters
        self.ep_limit = ep_limit
        self.initial_steps = initial_steps
        self.buffer_size = buffer_size 
        self.delay_step = delay_step
        self.eval_freq = eval_freq

        self.batch_size = batch_size
        self.mu_lr = mu_lr
        self.Q_lr = Q_lr
        self.noise_clip = noise_clip
        self.gamma = gamma
        self.sigma = sigma
        self.sigma_exp = sigma_exp
        self.tau = tau 

        self.device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

        self.actor = actor if actor is not None else Mu
        self.critic = critic if critic is not None else Q

        self.replay_memory = ReplayMemory(self.buffer_size, self.device)
        self.mean = []
        self.min = []
        self.max = []

        # Create environments
        self.env = env()
        self.env.seed(seed)
        self.env.action_space.seed(seed)
        self.eval_env = env()
        self.eval_env.seed(seed)
        self.eval_env.action_space.seed(seed)

        # Save action space and observation space of the environment
        obs_space = self.env.observation_space
        self.obs_space = obs_space.n if type(obs_space) == Discrete else obs_space.shape[0]
        self.act_space = self.env.action_space.shape[0] # TD3 does not support discrete action space
        self.act_low = torch.tensor(self.env.action_space.low, device=self.device)  
        self.act_high = torch.tensor(self.env.action_space.high, device=self.device)

        # Initialize networks and target networks (marked prime)
        self.mu = self.actor(self.obs_space, self.act_space).to(self.device)
        self.mu_prime = self.actor(self.obs_space, self.act_space).to(self.device)
        self.Q1 = self.critic(self.obs_space, self.act_space) .to(self.device)
        self.Q1_prime = self.critic(self.obs_space, self.act_space).to(self.device)
        self.Q2 = self.critic(self.obs_space, self.act_space) .to(self.device)
        self.Q2_prime = self.critic(self.obs_space, self.act_space).to(self.device)
        # Make target networks have same parameters in the beginning
        self.mu_prime.load_state_dict(self.mu.state_dict())
        self.Q1_prime.load_state_dict(self.Q1.state_dict())
        self.Q2_prime.load_state_dict(self.Q2.state_dict())

        # Optimizers for each network
        self.mu_optimizer = torch.optim.Adam(self.mu.parameters(), lr=self.mu_lr)
        self.Q1_optimizer = torch.optim.Adam(self.Q1.parameters(), lr=self.Q_lr)
        self.Q2_optimizer = torch.optim.Adam(self.Q2.parameters(), lr=self.Q_lr)


    def _soft_update(self, net, target_net):
        """Does a soft update to the target network using self.tau"""
        for target_param, param in zip(target_net.parameters(), net.parameters()):
            target_param.data.copy_(self.tau * param + (1 - self.tau) * target_param)


    def _optimize(self, t):
        """Optimizes parameters in the network

        This funciton is called multiple times during self.train() to calculate the loss and do
        backpropagation. Separated in own function for readability.
        """
        obs, act, rew, done, new_obs = self.replay_memory.sample(self.batch_size)

        noise = torch.normal(0., self.sigma, size=(self.batch_size, self.act_space)).to(self.device)
        noise = torch.clamp(noise, -self.noise_clip, self.noise_clip)
        act_estimate = self.mu_prime(new_obs) + noise
        # Clip into action range, torch.clamp does not support tensors yet.
        act_estimate = torch.max(torch.min(act_estimate, self.act_high), self.act_low)

        q1 = self.Q1_prime(new_obs, act_estimate)
        q2 = self.Q2_prime(new_obs, act_estimate)
        y = rew + self.gamma * torch.min(q1, q2) * ~done

        # Calculate loss
        critic1_loss = torch.nn.functional.mse_loss(y.detach(), self.Q1(obs, act))
        critic2_loss = torch.nn.functional.mse_loss(y.detach(), self.Q2(obs, act))

        # Update critic
        self.Q1_optimizer.zero_grad()
        critic1_loss.backward()
        self.Q1_optimizer.step()

        self.Q2_optimizer.zero_grad()
        critic2_loss.backward()
        self.Q2_optimizer.step()

        if t % self.delay_step == 0:
            # Update actor
            actor_loss = -self.Q1(obs, self.mu(obs)).mean()
            
            self.mu_optimizer.zero_grad()
            actor_loss.backward()
            self.mu_optimizer.step()

            # Update target networks
            self._soft_update(self.mu, self.mu_prime)
            self._soft_update(self.Q1, self.Q1_prime)
            self._soft_update(self.Q2, self.Q2_prime)
        

    def train(self, total_timesteps=100000):
        """Training loop for TD3

        Trains the TD3 agent with the hyperparameters defined in the init function. Will run for
        total_timesteps timesteps. Every episode will run until the environment terminates or 
        self.ep_timesteps has been reached.

        params:
            total_timesteps (int): Total number of timesteps to train for.
        """
        # Logging variables
        ep_timestep = 0
        self.total_timesteps = total_timesteps
        
        # Training loop
        obs = self.env.reset()
        for t in range(total_timesteps):
            ep_timestep += 1

            # Select action depending on state
            if t <= self.initial_steps:
                act = self.env.action_space.sample()
            else:
                obs_ten = torch.tensor(obs, dtype=torch.float32, device=self.device)
                act = self.mu(obs_ten).cpu().detach().numpy()
                act_noise = np.random.normal(0, self.sigma_exp, self.act_space)
                act = np.clip(act + act_noise, -1, 1)

            # Step environment
            new_obs, rew, done, _ = self.env.step(act)

            # Store transition
            self.replay_memory.put(obs, act, rew, done, new_obs)
            obs = new_obs

            # Optimize if initial exploration is done
            if t > self.initial_steps:
                self._optimize(t)

            # Evaluate agent from time to time
            if t % self.eval_freq == 0:
                self._evaluate(t)

            # Reset environment if it is done
            if done or ep_timestep > self.ep_limit:
                obs = self.env.reset()
                ep_timestep = 0

        # Final evaluation, NOTE will cause inconsistensies if total_timesteps % eval_freq != 0
        self._evaluate(total_timesteps)


    def _evaluate(self, t, eval_eps=10):
        """Evaluates the current state of the agent

        Evaluates the agent by calculating the average reward over 10 episodes using no exploration
        noise. Same way as the agent is evaluated in the original paper for TD3.

        params:
            t (int): The current timestep
        """
        rews = []

        for _ in range(eval_eps):
            obs = self.eval_env.reset()
            done = False
            score = 0

            while not done:
                obs = torch.tensor(obs, dtype=torch.float32, device=self.device)
                act = self.mu(obs).cpu().detach().numpy()
                obs, rew, done, _ = self.eval_env.step(act)
                score += rew
            
            rews.append(score)

        self.mean.append(np.mean(rews))
        self.min.append(np.min(rews))
        self.max.append(np.max(rews))
        print("Timestep: {}, avg return over {} episodes: {}".format(t, eval_eps, self.mean[-1]))


    def save(self):
        """Saves the models parameters in the folder model_data"""
        os.makedirs("model_data", exist_ok=True)
        torch.save(self.mu.state_dict(), "model_data/mu")
        torch.save(self.Q1.state_dict(), "model_data/q1")
        torch.save(self.Q2.state_dict(), "model_data/q2")


    def load(self):
        """Loads the models parameters from the folder model_data"""
        assert os.path.isdir("model_data"), "Cannot load model parameters, no parameters saved"
        self.mu.load_state_dict(torch.load("model_data/mu"))        
        self.Q1.load_state_dict(torch.load("model_data/q1"))
        self.Q2.load_state_dict(torch.load("model_data/q2"))

                
    def make_plot(self, filename="re_over_time"):
        """Creates a plot of the average reward over time and saves it"""
        x = np.linspace(0, self.total_timesteps, len(self.mean))
        fig, ax = plt.subplots(1)
        ax.plot(x, self.mean)
        ax.fill_between(x, self.min, self.max, alpha=0.2)
        
        # Save figure
        os.makedirs("results", exist_ok=True)
        plt.savefig("results/" + filename + ".png")


    def predict(self, obs):
        """Make a prediction

        Used to test the model after training is complete. This method will take in a state and
        output an action without using any exploration noise.

        params:
            obs (np.array): The observation recieved from the environment.
        returns:
            (np.array): The models prediction of the best action to take.
        """
        return self.mu(torch.tensor(obs, dtype=torch.float32, device=self.device)) \
                .cpu().detach().numpy()