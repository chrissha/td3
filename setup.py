from setuptools import setup, find_packages

setup(
    name="TD3",
    version="1.0",
    packages=find_packages(),
    scripts=["TD3.py"],
    install_requires=["torch", "torchvision", "gym", "matplotlib", "numpy"],
)

# Use:
#  pip install -e .
# Where . is the project directory