from TD3 import TD3

import gym

seed = 0

env = lambda: gym.make('Pendulum-v0') # Gym warning

model = TD3(env, seed=seed)
model.train(total_timesteps=25000)
model.make_plot()
model.save()

del model
model = TD3(env, seed=seed)
model.load()

env = env()
env.seed(seed)

# Test the trained model
for _ in range(5):
    obs = env.reset()
    while True:
        env.render()
        action = model.predict(obs)
        new_obs, rew, done, _ = env.step(action)
        obs = new_obs
        if done: break

env.close()